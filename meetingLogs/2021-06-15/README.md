This Week
=============
1. MadGraph v3.1 won't work with python older than v3.7. Jon commented last week that Herwig_repo_Rivet-repo will give python3 so I checked. It gave v3.6 so MadGraph still needs either newer python3 or python2 (v2.7 or higher).
	- This week I still used MadGraph v2.7 with python2.
	- [Olivier] python3 requirements (v3.7 or higher) was just because he never tested with old versions, but some people in CMS reported that it works.
	- [Jon] We can update the Herwig_repo_Rivet-repo's python versions.
2. MadGraph now gives us the heatmaps.
	- There's an issue with weight blocks [Weight_MERGING=0.000]. Removing it with using 'sed' command solved the issue.
	- [Jon] This could be solved easily with --wn option.
3. Plotting with |V|^2 on the y-axis instead of |V| was possible with REL mode.
3. Had questions regarding the interpolation level, whether it has something to do with the physics.
	- [Jon] It doesn't have any physics meaning, it's something that's related to the graphics of heatmaps.
4. Tested photon-induced VBF process with MadGraph.
	- This doesn't have sensitivity with Rivet results. Think this is because Rivet doesn't have any AK8 jet or same-sign dilepton results.
	- [Jon] Still this can steer people to implement AK8 jet or same-sign dilepton results to Rivet.
5. Some issue with MadGraph : 'failed to reach target'.
	- Required 30000 events but only ended up with 2575 events.
	- [Olivier] 1 -> 3 (N -> f f f) decay might be the cause of the problem. But anyways this might be fixed in MadGraph v3.1.
6. Some issue with Contur : Parameters not fully written in samples_pointed.dat.
	- slide7
7. MadGraph can easily blow up the storages as HepMC files are large in size.
	- [Jon] Use pipeline when running MadGraph instead of using the storages.

Remainining
=============
**To Do - fast**
1. Check whether UCLouvain servers are able to run Contur + Rivet.
	- [Olivier] ingrid-ui1 or manneback will be good to use, not gwceci.

**To Do - slow**
1. Have to learn how bzr works.
2. Check SM_HeavyN_NLO model file's Higgs BR.
3. Update the HeavyN page : https://hepcedar.gitlab.io/contur-webpage/results/HeavyN/index.html
	- [Jon] Keep it to ourselves for now (not yet upload it to the webpage). Some possibility that this can turn into paper work in the future, but of course everything becomes complicated when it becomes a paper work.
