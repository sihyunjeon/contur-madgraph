This Week
=============
1. Contur trial runs using Contur-MadGraph interface using UCL servers
2. Herwig can do N->3body decay, the results seem reasonable
3. MadGraph output looks fine (LHE, YODA) but Contur heatmap is not made
	- To run MadGraph some modifications needed, look for #FIXME in input/madgraph/run_batch_submit.py 

**To Do - fast**
1. Check /unix/cedar/software/cos7/Herwig-repo_Rivet-repo/ which will give python3 environments
2. Contur has functionality that can check individual Rivet results, see if Rivet plots from MadGraph looks fine
	- The results not shown in MadGraph but in Herwig might be different due to the different cross section units
3. Contur y-axis can give squared values (|V|^2) by setting some dummy indexes

**To Do - slow**
1. Have to learn how bzr works

Remainining
=============
**To Do - fast**
1. Check whether UCLouvain servers are able to run Contur + Rivet

**To Do - slow**
1. SM_HeavyN_NLO model file's Higgs BR seems to be incorrect : Possibly due to b mass settings (5f scheme in default)
2. Update the HeavyN page : https://hepcedar.gitlab.io/contur-webpage/results/HeavyN/index.html
