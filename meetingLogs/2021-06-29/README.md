This Week
=============
1. Suggested two things for running MadGraph inside Contur.
	1. Preparing MG setup shell script in Contur. Shared /unix/cedar/ directory seem to be possible to install MG as it tries to write something (make_opts) inside the MG repository.
	- [Olivier] make_opts doesn't neccessarilly need to be written, he can fix this.
	- [Jon] Don't really want some generator dependencies inside Contur. So let's still see if we can centrally use the shared repository.
	2. Fixing how Contur does batch submission for MG.
	- Batch submission script formats are not consistent with other generator (Herwig) so it is better to keep things consistent.
	- Use set of inputcards that MG takes instead of series of shell command lines.
	- Work on this whenever I have time.
  
2. python3 repo not there yet in UCL.
	- Herwig-7.2.2_Rivet-3.1.4-py3 worked fine but this is also v3.6.8 so had to remove 4-5 lines in MG.
	- python v3.6.8 also works for MG but not really recommended.
	- [Jon] Will set up a repo with py3 >= v3.7.
  
3. MG failed to reach target again happening.
	- [Olivier] Might be simply because the width is too big. Need to check the grids which failed.
  
4. pipe/fifo instead of HepMC worked fine.
  
5. Contur not drawing heatmaps was due to weight name issues. With --wn weightname option, worked fine.
  
  
Remainining
=============
**To Do - fast**
1. Run MadGraph using scan functionality, Pythia, and inject the outputs to Rivet and then Contur. Everything by hand, and if this works fine, would be a matter of linking the output files to the next steps.
	- Expect to get heatmaps from MG when the project is successful.
2. Check whether UCLouvain servers are able to run Contur + Rivet.
	- [Olivier] ingrid-ui1 or manneback will be good to use, not gwceci.
  
**To Do - slow**
1. Have to learn how bzr works.
2. Check SM_HeavyN_NLO model file's Higgs BR.
3. Update the HeavyN page : https://hepcedar.gitlab.io/contur-webpage/results/HeavyN/index.html
	- [Jon] Keep it to ourselves for now (not yet upload it to the webpage). Some possibility that this can turn into paper work in the future, but of course everything becomes complicated when it becomes a paper work.
